*** Settings ***
Documentation            Tests for demo
Resource                 ../Resources/keywords.robot
Resource                 ../Resources/locators.robot
Suite Setup              Start Suite
Suite Teardown           End Suite

*** Test Cases ***
Navigoi palvelu
    [Documentation]    Sisäänkirjautuminen
    [Tags]             Sign
    Appstate           Sign In
    ClickText          Palvelupyyntö
    ClickText          Luo uusi    Palvelupyyntö    
    ClickText          Ilmoittaja
    TypeText           Ilmoittaja     Leila Toive (1. taso)
    TypeText           Käyttäjä       Leila Toive (1. taso)
    ClickCheckbox      Tehtävän tarkistus vaaditaan    on
    TypeText           Palvelu    Gentax - Tuotanto
    TypeText           Palvelutarjooma    Gentax - SO-prod
    TypeText           Vastuuryhmä    Toive_UAT_IT 
    TypeText           Vastuuhenkilö    Jami Toive (1. taso)
    TypeText           Lyhyt kuvaus    testi lk    
    TypeText           Kuvaus   testi pk
    ClickText          Tallenna
    
    
    



    