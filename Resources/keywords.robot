*** Settings ***
Documentation    Doku
Library          QWeb
Library          String

*** Variables ***
${BROWSER}       chrome


*** Keywords ***
Start Suite
    OpenBrowser    about:blank    ${BROWSER}
    SetConfig      SearchMode     draw

End Suite
    CloseAllBrowsers

AppState
    [Documentation]   Checks which actions should be taken prior to testing
    [Arguments]       ${state}
    ${state}          Convert To Lowercase    ${state}
    Run Keyword If    '${state}'=='homepage'
    ...               Homepage
    Run Keyword If    '${state}'=='sign in'
    ...               Sign In
    
Homepage
    GoTo             ${URL}
    VerifyText       Käyttäjänimi
    VerifyText       Salasana

Sign In
    Homepage
    ${signed_in}=    IsText                    Sign out
    TypeText         Käyttäjänimi              nico.lundqvist
    TypeSecret       Salasana                  ${PASSWORD}
    ClickText        Sisäänkirjautuminen
    VerifyText       Itsepalvelu
   
    